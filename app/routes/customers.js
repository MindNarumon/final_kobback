const express = require('express')
const router = express.Router()
const customersController = require('../controllers/Customers.controller')

/* GET customers listing. */
router.get('/', customersController.getCustomers)

router.get('/:id', customersController.getCustomer)

router.post('/', customersController.addCustomer)

router.put('/', customersController.updateCustomer)

router.delete('/:id', customersController.deleteCustomer)

module.exports = router